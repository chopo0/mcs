﻿using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using MovChat.Data.Repositories;
using MovChat.Email;
using MoveleirosChatServer.Models;
using MoveleirosChatServer.Data;
using MovChat.Data;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System.Collections.Generic;
namespace MoveleirosChatServer.Controllers
{
    public class FormsController : Controller
    {
		private readonly LivechatContext context;
		private readonly UOW uow;
        private readonly IHostingEnvironment environment;
		private readonly LivechatRules livechatRules;

		public FormsController(
            UOW uow,
			LivechatRules livechatRules,
			LivechatContext context,
			IHostingEnvironment environment)
        {
            this.uow = uow ?? throw new System.ArgumentNullException(nameof(uow));
            this.environment = environment ?? throw new System.ArgumentNullException(nameof(environment));
			this.livechatRules = livechatRules;
			this.context = context ?? throw new System.ArgumentNullException(nameof(context));
		}

        /// <summary>
        /// This method receives a question form to be sent.
        /// </summary>
        /// <param name="questionForm">Form to be sent</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> SendQuestionForm([FromBody] QuestionFormData questionForm)
        {
			/*var userDetails = await CurrentUserDetails();
			var userStoreId = userDetails.CustomerStoreId;
			var userCurrentChannel = await livechatRules.GetCustomerChannel(
				userStoreId,
				userDetails.LivechatUserId
			);
			var storeRepository = uow.GetRepository<StoreRepository>();
			var store = await storeRepository.GetAICredentials(userStoreId);
			var city = await storeRepository.GetCity(store.CityId);
			extraPayload.Add("nome", userDetails.Payload["name"]);
			extraPayload.Add("telefone", userDetails.Payload["phone"]);
			extraPayload.Add("loja", userDetails.Payload["currentStore"]);
			extraPayload.Add("store", store);
			extraPayload.Add("city", city);
			extraPayload["email"] = userDetails.Payload["email"];
			var userRepository = uow.GetRepository<UserRepository>();

			//userDetails.Payload[3] = 
			await userRepository.SetNewUserPayloadFromWidget(
				channelId: userCurrentChannel.ChannelId,
				newPayload: extraPayload,
				exceptUserId: userDetails.LivechatUserId);

			await CurrentUser.SendAsync(HubMessages.EDIT_CUSTOMER_PROFILE_SAVED); */


			// Get seller emails list
			var userStoreId = questionForm.StoreId;
			var userCurrentChannel = await livechatRules.GetCustomerChannel(
				userStoreId,
				questionForm.LivechatUserId
			);
			var user = await context.LivechatChannelUser
				.Where(t => t.LivechatUserId == questionForm.LivechatUserId &&
					t.LivechatChannelId == userCurrentChannel.ChannelId)
				.FirstOrDefaultAsync();
			if (user != null)
			{
				var userRepository = uow.GetRepository<UserRepository>();
				var payload = JsonConvert.DeserializeObject<Dictionary<string, object>>(user.Payload); ;
				payload["comments"] = questionForm.Message;
				await userRepository.AddPayloadToUser(
					livechatUserId: user.LivechatUserId,
					channelId: userCurrentChannel.ChannelId,
					payload: payload
				);
			}
			var storeRepository = uow.GetRepository<StoreRepository>();
			var adminLojaEmails = await storeRepository.GetAdminsEmailFromStore(questionForm.StoreId);

            // Send an email for each person in store
            var emailService = new SendGridEmail("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");

            // Read email template
            var emailTemplate = string.Empty;
            using (var reader = new StreamReader(Path.Combine(environment.ContentRootPath, "EmailTemplates", "client_without_agent.html")))
            {
                emailTemplate = await reader.ReadToEndAsync();
            }

            // Send emails
            var tempEmailTemplate = string.Empty;
            foreach (var email in adminLojaEmails)
            {
                tempEmailTemplate = emailTemplate
                    .Replace("{{name}}", questionForm.Name)
                    .Replace("{{contact}}", questionForm.Phone)
                    .Replace("{{message}}", questionForm.Message);

                await emailService.EnviarEmail(
                    to: email.Email,
                    header: tempEmailTemplate,
                    subject: "Cliente sem atendimento!",
                    fromAddress: "no-reply@moveleiros.com.br",
                    name: "Chat Moveleiros",
                    textPlain: $"O cliente {questionForm.Name} não foi atendido. " +
                        $"O contato informado é {questionForm.Phone}. " +
                        $"A mensagem deixada foi {questionForm.Message}"
                );
            }

            return Ok();
        }
    }
}