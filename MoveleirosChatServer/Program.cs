﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;

namespace MoveleirosChatServer
{
    public class Program
    {
        public static void Main(string[] args)
        {
            BuildWebHost(args).Run();
        }

        public static IWebHost BuildWebHost(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>()
                // .UseUrls("http://192.168.1.78:59471", "http://localhost:59471")
                .Build();
    }
}
