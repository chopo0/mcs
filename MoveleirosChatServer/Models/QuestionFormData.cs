﻿namespace MoveleirosChatServer.Models
{
    public class QuestionFormData
    {
        public string Name { get; set; }
        public string Phone { get; set; }
        public string Message { get; set; }
        public int StoreId { get; set; }
		public string LivechatUserId { get; set; }
		public string CustomerId { get; set; }

	}
}
